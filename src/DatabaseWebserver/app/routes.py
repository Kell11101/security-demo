from app import app, db
from flask import render_template, redirect, send_file, request, url_for
from app.models import User
import requests

@app.route("/", methods=["GET", "POST"])
@app.route("/index", methods=["GET", "POST"])
def index():
    return "Its's Working!"

#Both /resource/ pages should be moved to a second server to better
#simulate the SSRF vulnerability as these are the resources being
#requested by this server
@app.route('/resource/image/<resource>')
def getImage(resource):
    return send_file('static/' + resource)

@app.route("/admin")
def admin():
    users = User.query.all()
    return render_template("admin.html", users=users)

@app.route("/validate")
def validate():
    if request.args:
        if 'username' in request.args:
            user = User.query.filter_by(username=request.args['username']).first()
            if user is not None:
                return "Username exists"
    return "Username not exists"

@app.route("/delete")
def delete():
    if request.args:
        if 'username' in request.args:
            user = User.query.filter_by(username=request.args['username']).first()
            if user is not None:
                db.session.delete(user)
                db.session.commit()
                return "Delete successful"
    return "Delete unsuccessful"

@app.route("/login")
def login():
    if request.args:
        if 'username' in request.args and 'password' in request.args:
            user = User.query.filter_by(username=request.args['username']).first()
            if user is not None and user.check_password(request.args['password']):
                return "Authenticated"
    return "Not authenticated"


@app.route("/register")
def register():
    if request.args:
        if 'username' in request.args and 'password' in request.args:
            user = User.query.filter_by(username=request.args['username']).first()
            if user is not None:
                return "Duplicate registration"

            user = User(username=request.args['username'])
            user.set_password(request.args['password'])
            db.session.add(user)
            db.session.commit()
            return "Register successful"
    return "Register unsuccessful"

@app.route("/isadmin")
def is_admin():
    if request.args:
        if 'username' in request.args and 'password' in request.args:
            username = request.args['username']
            password = request.args['password']
            if requets.get(url_for("/login?username=" + username + "&password=" + password) == "Authenticated"):
                user = User.query.filter_by(username=request.args['username']).first()
                if user.is_admin == True:
                    return "Access granted"
                else:
                    return "Access denied"


@app.route("/modify")
def make_admin():
    if request.args:
        if 'username' in request.args:
            username = request.args['username']
            user = User.query.filter_by(username=request.args['username']).first()
            if 'admin' in request.args:
                user.is_admin = eval(request.args['admin'])
                db.session.commit()
    return redirect(url_for("admin"))