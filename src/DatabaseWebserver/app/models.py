from app import db
from datetime import datetime
from sqlalchemy.sql import text, expression
from sqlalchemy.exc import OperationalError
from sqlalchemy import Column, Integer, String, Boolean

class User(db.Model):
    id = Column(Integer, primary_key=True)
    username = Column(String(64), index=True, unique=True)
    password = Column(String(64))
    picture = Column(String(64))
    is_admin = Column(Boolean, server_default=expression.false(), nullable=False)
    __searchable__ = ["username"]

    def set_password(self, password):
        self.password = password

    def check_password(self, password):
        return self.password == password

    def __repr__(self):
        return "<User {}>".format(self.username)