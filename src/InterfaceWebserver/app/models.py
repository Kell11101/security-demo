from flask_login import UserMixin
from app import login
import requests

@login.user_loader
def load_user(username, password):
    api = "http://127.0.0.1:5001/login?username=" + username + "&password=" + password
    response = requests.get(api)

    if response.content == "Authenticated":
        user = User()
        user.username = username
        user.password = password
        return user
    else:
        return None

class User(UserMixin):
    def __init__(self, username, password):
        super().__init__()
        self.username = username
        self.password = password