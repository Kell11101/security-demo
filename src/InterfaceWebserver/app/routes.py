from app import app
import requests
from flask import render_template, flash, redirect, url_for
from flask_login import current_user, login_user, login_required, logout_user
from app.models import User
from app.forms import LoginForm, RegistrationForm

@app.route("/", methods=["GET", "POST"])
@app.route("/index", methods=["GET", "POST"])
@login_required
def index():
    return render_template("index.html", title="CatBook | Home", form = form, posts=posts)

@app.route("/register", methods=["GET", "POST"])
def register():
    if current_user.is_authenticated:
        return redirect(url_for("index"))
    
    form = RegistrationForm()
    if form.validate_on_submit():
        username = form.username.data
        password = form.password.data

        if requests.get("http://127.0.0.1:5001/validate?username=" + username).content == "Username exists":
            flash("Username is taken")
            return render_template("register.html", title="CatBook | Register", form=form)
        else:
            requests.get("http://127.0.0.1:5001/register?username=" + username + "&password=" + password)
            flash("Welcome to CatBook")
            return redirect(url_for("login"), form=LoginForm())


@app.route("/login", methods=["GET", "POST"])
def login():
    if current_user.is_authenticated:
        return redirect(url_for("index"))

    form = LoginForm()
    if form.validate_on_submit():
        username = form.username.data
        password = form.password.data

        api = "http://127.0.0.1:5001/login?username=" + username + "password=" + password
        response = requests.get(api)

        if response.content == "Not authenticated":
            flash("Invalid username or password")
            return redirect(url_for("login"))
        else:
            user = User()
            user.username = username
            user.password = password
            login_user(user, remember=true)
            return redirect(url_for("index"))

    return render_template("login.html", title="CatBook | Login", form=form)

@app.route("/logout")
def logout():
    logout_user()
    return redirect(url_for("index"))

@app.route("/admin")
def admin():
    api = "http://127.0.0.1:5001/isadmin?username=" + current_user.username + "&password=" + current_user.password
    response = requests.get(api)

    if response.content == "Access granted":
        return requests.get("http://127.0.0.1:5001/admin").content
    else:
        return render_template("403.html"), 403